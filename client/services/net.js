var auth = require('./auth');
var io = require('socket.io-client');
var EventEmitter = require('eventemitter3');

function Net() {
    this.events = new EventEmitter();
    this.io = io(undefined, {
        autoConnect: false,
        reconnection: false
    });
    this.status = 'connecting'
}

Net.prototype.connect = function() {
    console.log('doing connect');
    this.io.removeAllListeners();
    this.io.on('connect', this.sendAuthRequest.bind(this));
    this.io.on('authSuccess', this.onAuthSuccess.bind(this));
    this.io.on('disconnect', this.onDisconnect.bind(this));
    this.io.on('joinSuccess', (data) => {
        this.events.emit('joinSuccess', data);
        this.status = 'playing'
    });
    this.io.on('worldUpdate', (data) => {
        if (this.status === 'playing')
            this.events.emit('worldUpdate', data);
        else
            console.log('dropping world update, not joined yet')
    });

    this.io.open();
};

Net.prototype.disconnect = function() {};

Net.prototype.onServerCmd = function() {};

Net.prototype.sendAuthRequest = function() {
    console.log('connected, sending auth request');
    var self = this;
    auth.getCurrentUser().getIdToken().then(function(token) {
        self.io.emit('authRequest', token);
    });
};

Net.prototype.send = function(evt, data) {

    console.log(evt, data);
    this.io.emit(evt, data);
};

Net.prototype.onAuthSuccess = function() {
    console.log('auth success');
    this.events.emit('connected');


};
Net.prototype.onDisconnect = function() {
    console.log('net dc, reloading');
    setTimeout(function() {
        window.location.reload(true);
    }, 1500);
}

module.exports = new Net();