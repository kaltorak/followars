var EventEmitter = require('eventemitter3')
var events = new EventEmitter()
//TODO, might want to move the auth into it's own small bundle so it can start authing while the main client bundle loads

var firebase = require("firebase/app")
require("firebase/auth")

var config = {
    //redacted firebase configs
}

firebase.initializeApp(config)

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        // var displayName = user.displayName
        // var isAnonymous = user.isAnonymous
        // var uid = user.uid
        events.emit('signin', user)
    } else {
        // User is signed out.
        events.emit('signout', null)
    }
})

firebase.auth().signInAnonymously().catch(function(error) {
    console.log('auth error', error)
})

module.exports = {
    events: events,
    getCurrentUser: function() {
        return firebase.auth().currentUser
    }
}