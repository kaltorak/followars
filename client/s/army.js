function Army(scene) {
    this.ents = scene.ents
    this.scene = scene
    this.events = scene.events
    this.events.on('entCreated', this.onEntCreated.bind(this))
    this.events.on('entRemoving', this.onEntRemoving.bind(this))
    this.events.on('entRemoved', this.onEntRemoved.bind(this))

    this.shortlist = []

    this.events.on('joinSuccess', (data) => {
        console.log(data)
    })
}

Army.prototype.update = function(delta) {

}

Army.prototype.onEntCreated = function(ent) {
    if (ent.type === 'creep') {
        this.shortlist.push(ent)
    }
}

Army.prototype.onEntRemoving = function(ent) {
}

Army.prototype.onEntRemoved = function(ent) {
    let idx = this.shortlist.indexOf(ent)
    if (idx > -1)
        this.shortlist.splice(idx, 1)
}

module.exports = Army