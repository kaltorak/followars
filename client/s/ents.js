var _ = require('lodash')
var Ent = require('../e/ent')
var Types = require('../../server/ents/types')
var Templates = require('../e/templates')

function Ents(scene) {
    this.events = scene.events
    this.ents = []
    this.byId = {}
    this.indexes = {}
    this.scaleFactor = 30
    this.scalables = {
        x: true,
        y: true,
        r: true,
        w: true,
        h: true,
        width: true,
        height: true
    }
    window.ents = this
}

Ents.prototype.make = function(type, options) {
    options.type = options.type || type
    var ent = new Ent(_.merge(_.clone(Types[type] || Templates[type]), options))

    this.ents.push(ent)
    this.byId[ent.id] = ent;
    this.events.emit('entCreated', ent)
    return ent
}

Ents.prototype.remove = function(ent) {
    if (typeof ent === 'string')
        ent = this.byId[ent]
    ent._dead = true
    this.events.emit('entRemoving', ent)
    let idx = this.ents.indexOf(ent)
    if (idx > -1) {
        this.ents.splice(idx, 1);
    }

    delete this.byId[ent.id]
    this.events.emit('entRemoved', ent)
}

Ents.prototype.setBy = function(key, val) {
    if (!this.indexes[key]) this.indexes[key] = []
    this.indexes[key].push(val)
}

Ents.prototype.getBy = function(key) {
    return this.indexes[key] || []
}

Ents.prototype.removeBy = function(key, val) {
    if (!this.indexes[key]) return
    let idx = this.indexes[key].indexOf(val)
    if (idx > -1) {
        this.indexes[key].splice(idx, 1);
    }
}

Ents.prototype.clearBy = function(key) {}

module.exports = Ents;