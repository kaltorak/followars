let renderMap = {
    avatar: require('./render/avatar'),
    obstacle: require('./render/obstacle'),
    spawn: require('./render/spawn'),
    bounds: require('./render/bounds'),
    soldier: require('./render/soldier'),
    archer: require('./render/archer'),
    projectile: require('./render/projectile'),
    healthbar: require('./render/healthbar'),
}

function Render(scene) {
    this.ents = scene.ents
    this.scene = scene
    this.events = scene.events
    this.events.on('entCreated', this.onEntCreated.bind(this))
    this.events.on('entRemoving', this.onEntRemoving.bind(this))
    this.events.on('entRemoved', this.onEntRemoved.bind(this))

    this.events.on('netUpdate', this.netUpdate.bind(this))

    this.shortlist = []
    let namespace = 1000
    this.layers = {
        bg: 1,
        bounds: .5 * namespace,
        obstacle: 11 * namespace,
        spawn: 3 * namespace,
        soldier: 4 * namespace,
        archer: 5 * namespace,
        fx: 12 * namespace,

        avatar: 10 * namespace,

    }
}

Render.prototype.netUpdate = function(ent, data) {
    _.merge(ent, data)

    ent.netStatePrev = ent.netState
    ent.netState = data
    if (ent.type === 'soldier') {
        if (Math.hypot(ent.x - this.ents.me.x, ent.y - this.ents.me.y) > this.ents.me.voiceRadius + ent.r + this.ents.me.r) {
        }
    }
    if (data.ownerId && renderMap[ent.type])
        renderMap[ent.type].create.call(this.scene, ent)

    if (data.hasOwnProperty('hp'))
        renderMap.healthbar.update.call(this.scene, ent)
}

Render.prototype.emitGood = function(pos) {}
Render.prototype.emitBad = function(pos) {}

const TICK_RATE = 1 / 60
Render.prototype.update = function(delta) {

    let interp = delta * TICK_RATE
    let ent
    let spr
    for (let i = this.shortlist.length; i--;) {
        ent = this.shortlist[i]
        if (ent.type === 'spawn' && ent.ownerId === this.ents.myId) {
            ent.cooldown -= delta / 1000
            if (renderMap[ent.type].update)
                renderMap[ent.type].update(ent, delta)

        }

        if (ent.netStatePrev.x && ent.netState.x || ent.netStatePrev.y && ent.netState.y) {
            for (let s = ent._sprites.length; s--;) {
                spr = ent._sprites[s]
                if (ent.netStatePrev.x && ent.netState.x)
                    spr.x = ent.netStatePrev.x + (ent.netState.x - ent.netStatePrev.x) * interp
                if (ent.netStatePrev.y && ent.netState.y)
                    spr.y = ent.netStatePrev.y + (ent.netState.y - ent.netStatePrev.y) * interp

            }
        }
    }
}

Render.prototype.callIfChanged = function(ent, prop, fn, replace) {
    if (ent.netState[prop] && ent.netState[prop] !== ent[prop]) {
        if (replace) {
            ent[prop] = ent.netState[prop]
            ent.netState[prop] = undefined
        }
        fn.call(this.scene, ent)
    }
}

Render.prototype.onEntCreated = function(ent) {
    ent._sprites = []
    ent._layers = this.layers
    ent._layer = this.layers[ent.type] += 1
    if (renderMap[ent.type])
        renderMap[ent.type].create.call(this.scene, ent)
    if (ent.hasOwnProperty('hp'))
        renderMap.healthbar.create.call(this.scene, ent)
    if (ent._sprites.length)
        this.shortlist.push(ent)
}

Render.prototype.onEntRemoving = function(ent) {}

Render.prototype.onEntRemoved = function(ent) {
    _.each(ent._sprites, s => {
        s.ent = null
        s.destroy()
    })
    ent._sprites.length = 0
    let idx = this.shortlist.indexOf(ent)
    if (idx > -1)
        this.shortlist.splice(idx, 1)
}

module.exports = Render