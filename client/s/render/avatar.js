module.exports = {
    create: function(ent) {
        console.log('create', ent.type, ent)
        let body = _.find(ent._sprites, s => s._renderId === 'body')
        if (!body) {
            body = this.add.sprite(ent.x, ent.y, 'a' + _.random(1, 21)).setDepth(ent._layer).setDisplaySize(ent.r * 2, ent.r * 2)
            body._renderId = 'body'
            ent._sprites.push(body)
        }
    },
    draw: function() {},
    update: function(ent, delta) {},
    remove: function(ent) {}

};