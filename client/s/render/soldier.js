module.exports = {
    create: function(ent) {
        // console.log('create', ent.type, ent)
        // this.make.graphics()
        //     .fillStyle(0x992222).fillCircle(ent.r + 1, ent.r + 1, ent.r)
        //     .generateTexture(ent.id, ent.r * 2 + 2, ent.r * 2 + 2)
        let body = this.add.sprite(ent.x, ent.y, _.sample(['apple_happy', 'grape_happy', 'strawberry_happy'])).setDepth(ent._layer + .1).setDisplaySize(ent.r * 2, ent.r * 2).setAlpha(.8)
        body._renderId = 'body'
        ent._sprites.push(body)

        ent._sprites.push(this.add.sprite(ent.x, ent.y, 'bubble').setDepth(ent._layer).setDisplaySize(ent.r * 2.8, ent.r * 2.8).setTint(0xeeffee))
    },
    draw: function() {},
    update: function(ent, delta) {
        
        
        
    },
    remove: function(ent) {}

};