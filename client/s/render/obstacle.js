module.exports = {
    create: function(ent) {
        console.log('create obstacle', ent.id)
        if (this.textures.exists(ent.id)) return
        if (!this.cloudcache) {
            this.cloudcache = {}
            for (let i = 1; i < 8; i++)
                this.cloudcache[i] = this.add.image(-9999, -9999, 'cloud' + i).setOrigin(.5)
        }
        let minx = Number.MAX_VALUE
        let maxx = Number.MIN_VALUE
        let miny = Number.MAX_VALUE
        let maxy = Number.MIN_VALUE
        _.each(ent.geom, s => {
            if (s.x < minx) minx = s.x
            if (s.x > maxx) maxx = s.x
            if (s.y < miny) miny = s.y
            if (s.y > maxy) maxy = s.y
        })
        _.each(ent.geom, s => {
            s.x -= minx - 2
            s.y -= miny - 2
        })
        let padding = 60
        let poly = new Phaser.Geom.Polygon(ent.geom)
        Phaser.Geom.Polygon.Smooth(poly)
        let rt = new Phaser.GameObjects.RenderTexture(this, 0, 0, maxx - minx + padding, maxy - miny + padding)
        _.each(poly.points, pt => {
            rt.draw(this.cloudcache[_.sample([2, 7])], pt.x + padding / 2, pt.y + padding / 2)
            rt.draw(this.cloudcache[_.sample([2, 7])], pt.x + padding / 2, pt.y + padding / 2)
        })
        for (let x = 0; x < maxx - minx; x += 30) {
            for (let y = 0; y < maxy - miny + padding / 2; y += 30) {
                if (poly.contains(x, y) && Math.random() > .1) {
                    rt.draw(_.sample(this.cloudcache), x + padding / 2, y + padding / 4)
                    // if (Math.random() > .7) {
                    //     let l = this.add.sprite(ent.x + x + padding / 2, ent.y + y + padding / 4, 'lightning')
                    //         .setFlipX(!!Math.round(Math.random()))
                    //         .setOrigin(.5, 0)
                    //         .setDepth(ent._layer + 1)
                    //     l._renderId = 'lightning'
                    //     ent._sprites.push(l)
                    // }
                }
            }
        }

        rt.saveTexture(ent.id)

        let spr = this.add.sprite(ent.x, ent.y, ent.id).setDepth(ent._layer)
        spr._renderId = 'body'
        ent._sprites.push(spr)
    },
    draw: function() {},
    update: function(ent, delta) {},
    remove: function(ent) {}

};