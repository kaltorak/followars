let colors = {
    friendly: {
        5: 0xffffff,
        4: 0xe8e8e8,
        3: 0xeeeeee,
        2: 0xd8d8d8,
        1: 0xd0d0d0,
        0: 0xc8c8c8,
    },
    enemy: {
        0: 0xc8a8a8,
        1: 0xc0a0a0,
        2: 0xb89898,
        3: 0xb09090,
        4: 0xa88888,
        5: 0xa08080,
    }
}

module.exports = {
    create: function(ent) {
        // console.log('create health', ent.type)
        //new Arc(scene [, x] [, y] [, radius] [, startAngle] [, endAngle] [, anticlockwise] [, fillColor] [, fillAlpha])

        // this.make.graphics()
        //     .fillStyle(ent.ownerId === this.ents.myId ? 0x444499 : color).fillCircle(ent.r + 5, ent.r + 5, ent.r)
        //     .lineStyle(5, 0x448844)
        //     .beginPath()
        //     .arc(ent.r + 5, ent.r + 5, ent.r, 0, Math.PI * 2 * (ent.hp / ent.maxHp))
        //     .strokePath()
        //     .generateTexture(ent.id, ent.r * 2 + 10, ent.r * 2 + 10)

        // this.make.graphics().lineStyle(3, 0xbbaaaa)
        //     .strokeRect(ent.x, ent.y, ent.w, ent.h)
        //     .generateTexture(ent.id, ent.w, ent.h)

        if (ent.geom) {

            for (let i = 0; i < ent._sprites.length; i++) {
                const spr = ent._sprites[i]
                if (spr._renderId === 'body')
                    spr.setTint(colors[ent.ownerId === this.ents.myId ? 'friendly' : 'enemy'][Math.round(ent.hp)])
            }
        } else {
            let hb = this.add.arc(ent.x, ent.y, ent.r, 0, 360 * (ent.hp / ent.maxHp), false)
                .setAngle(-90).setClosePath(false).setStrokeStyle(4, 0x448844)
            hb.setDepth(ent._layer + .9)
            hb._renderId = 'healthbar'
            ent._sprites.push(hb)
        }

    },
    draw: function() {},
    update: function(ent) {
        if (ent.geom) {
            for (let i = 0; i < ent._sprites.length; i++) {
                const spr = ent._sprites[i]
                if (spr._renderId === 'body') {
                    // console.log('update hp', colors[ent.ownerId === this.ents.myId ? 'friendly' : 'enemy'][Math.round(ent.hp)])
                    spr.setTint(colors[ent.ownerId === this.ents.myId ? 'friendly' : 'enemy'][Math.round(ent.hp)])
                }
            }
        } else {
            let hb = _.find(ent._sprites, s => s._renderId === 'healthbar')
            if (hb)
                hb.setEndAngle(360 * (ent.hp / ent.maxHp))
        }
    },
    remove: function(ent) {}
};