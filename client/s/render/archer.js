module.exports = {
    create: function(ent) {
        console.log('create', ent.type, ent)
        this.make.graphics()
            .fillStyle(0x222299).fillCircle(ent.r + 1, ent.r + 1, ent.r)
            .generateTexture(ent.id, ent.r * 2 + 2, ent.r * 2 + 2)
        ent._sprites.push(this.add.sprite(ent.x, ent.y, ent.id).setDepth(ent._layer))
    },
    draw: function() {},
    update: function(ent, delta) {},
    remove: function(ent) {}

};