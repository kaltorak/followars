module.exports = {
    create: function(ent) {
        console.log('create spawn', ent.id)
        let body = ent.findSprite('body')
        if (body) {
            if (ent.ownerId === this.ents.myId)
                body.setTint(0x88ff88)
            else body.clearTint()
        } else {
            body = this.add.sprite(ent.x, ent.y, 'bubble').setDepth(ent._layer).setDisplaySize(ent.r * 2, ent.r * 2)
            if (ent.ownerId === this.ents.myId)
                body.setTint(0x88ff88)
            else body.clearTint()
            body._renderId = 'body'
            ent._sprites.push(body)
            let cooldown = this.add.image(ent.x, ent.y, 'heart_big').setAlpha(1).setScale(10 - ent.cooldown)
            cooldown._renderId = 'cooldown'
            ent._sprites.push(cooldown)
        }
    },
    draw: function(ent) {},
    update: function(ent, delta) {
        let cool = ent.findSprite('cooldown')
        if (cool) {
            cool.setScale(1 - (ent.cooldown / 10))
        }
    },
    remove: function(ent) {}

};