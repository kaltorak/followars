module.exports = {
    create: function(ent) {
        console.log('create', ent.type)
        this.make.graphics().lineStyle(3, 0xbbaaaa)
            .strokeRect(ent.x, ent.y, ent.w, ent.h)
            .generateTexture(ent.id, ent.w, ent.h)
        ent._sprites.push(this.add.sprite(ent.x, ent.y, ent.id).setDepth(ent._layer).setOrigin(0))
    },
    draw: function() {},
    update: function(ent, delta) {},
    remove: function(ent) {}
};