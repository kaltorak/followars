var auth = require('./services/auth');
var Phaser = require('phaser');
window.Phaser = Phaser;
window._ = require('lodash');
require('./services/net')

window.w = window.innerWidth
window.h = window.innerHeight
var config = {
    type: Phaser.AUTO,
    width: window.w,
    height: window.h,
    transparent: true,
    antialias: true,
    parent: 'container',
    scene: [
        require('./scenes/play')
    ]
};
auth.events.on('signin', function(user) {
    //try to signin if user has been here before
    //if no browser cache login, signin anonymously and offer choice to signin or signup when selecting game
    console.log('signin', user);
    let g = new Phaser.Game(config);
    window.gm = g

});

(function(window, document) {
    var prefix = "",
        _addEventListener, support;
    if (window.addEventListener) {
        _addEventListener = "addEventListener";
    } else {
        _addEventListener = "attachEvent";
        prefix = "on";
    }
    support = "onwheel" in document.createElement("div") ? "wheel" : // Modern browsers support "wheel"
        document.onmousewheel !== undefined ? "mousewheel" : // Webkit and IE support at least "mousewheel"
        "DOMMouseScroll"; // let's assume that remaining browsers are older Firefox

    window.addWheelListener = function(elem, callback, useCapture) {
        _addWheelListener(elem, support, callback, useCapture);
        if (support == "DOMMouseScroll") {
            _addWheelListener(elem, "MozMousePixelScroll", callback, useCapture);
        }
    };

    function _addWheelListener(elem, eventName, callback, useCapture) {
        elem[_addEventListener](prefix + eventName, support == "wheel" ? callback : function(originalEvent) {
            !originalEvent && (originalEvent = window.event);
            var event = {
                originalEvent: originalEvent,
                target: originalEvent.target || originalEvent.srcElement,
                type: "wheel",
                deltaMode: originalEvent.type == "MozMousePixelScroll" ? 0 : 1,
                deltaX: 0,
                deltaY: 0,
                deltaZ: 0,
                preventDefault: function() {
                    originalEvent.preventDefault ?
                        originalEvent.preventDefault() :
                        originalEvent.returnValue = false;
                }
            };
            if (support == "mousewheel") {
                event.deltaY = -1 / 40 * originalEvent.wheelDelta;
                originalEvent.wheelDeltaX && (event.deltaX = -1 / 40 * originalEvent.wheelDeltaX);
            } else {
                event.deltaY = originalEvent.deltaY || originalEvent.detail;
            }
            return callback(event);

        }, useCapture || false);
    }

})(window, document)