var shortid = require('shortid')

function Ent(options) {
    _.merge(this, options)
    this.id = options.id || shortid.generate()
    this.name = options.name || 'DEFAULT NAME'
    this.type = options.type || 'DEFAULT TYPE'
    this.netState = {} //latest server state
    this.netStatePrev = {} //previous server state so we have the other end of the interpolation
    return this
}

Ent.prototype.findSprite = function(renderId) {
    return _.find(this._sprites, ['_renderId', renderId])
}

Ent.prototype.findSprites = function(renderId) {
    return _.filter(this._sprites, s => s._renderId ? s._renderId.indexOf(renderId) > -1 : false)
}

Ent.prototype.destroySprite = function(renderId) {
    let result = this.findSprite(renderId)
    if (result) {
        _.remove(this._sprites, s => s === result)
        result.destroy()
    }
}

module.exports = Ent