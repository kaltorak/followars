let PL = require('planck-js')
let Vec2 = PL.Vec2

let Ents = require('../s/ents')
let Army = require('../s/army')
let Render = require('../s/render')
let net = require('../services/net')

module.exports = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function play() {
        Phaser.Scene.call(this, {
            key: 'play'
        })
    },

    preload: function() {
        this.load.image('avatar', 'img/avatar.png')
        this.load.image('line', 'img/line.png')
        this.load.image('bubble', 'img/bubble.png')
        this.load.image('heart', 'img/heart.png')
        this.load.image('heart_big', 'img/heart_big.png')
        this.load.image('heart_white', 'img/heart_white.png')
        this.load.image('heart_broken_left', 'img/heart_broken_left.png')
        this.load.image('heart_broken_right', 'img/heart_broken_right.png')
        this.load.image('heart_broken_left_white', 'img/heart_broken_left_white.png')
        this.load.image('heart_broken_right_white', 'img/heart_broken_right_white.png')

        this.load.image('cloud1', 'img/cloud1.png')
        this.load.image('cloud2', 'img/cloud2.png')
        this.load.image('cloud3', 'img/cloud3.png')
        this.load.image('cloud4', 'img/cloud4.png')
        this.load.image('cloud5', 'img/cloud5.png')
        this.load.image('cloud6', 'img/cloud6.png')
        this.load.image('cloud7', 'img/cloud7.png')
        this.load.image('lightning', 'img/lightning.png')
        this.load.image('sky', 'img/sky.png')
        this.load.image('heart', 'img/heart.png')
        this.load.image('heart_white', 'img/heart_white.png')

        this.load.image('grape_sad', '/img/grape_sad.png')
        this.load.image('strawberry_sad', '/img/strawberry_sad.png')
        this.load.image('apple_sad', '/img/apple_sad.png')
        this.load.image('grape_happy', '/img/grape_happy.png')
        this.load.image('apple_happy', '/img/apple_happy.png')
        this.load.image('strawberry_happy', '/img/strawberry_happy.png')

        for (let i = 1; i < 21; i++) {
            this.load.image(`a${i}`, `img/avatars/a${i}.png`)
        }
        this.load.image('crown', `img/avatars/crown.png`)
        this.load.image('crown', `img/avatars/anon.png`)

    },

    create: function() {
        this.ents = new Ents(this)
        this.army = new Army(this)
        this.render = new Render(this)

        net.events.on('joinSuccess', this.onJoinSuccess.bind(this))
        net.events.on('worldUpdate', this.onWorldUpdate.bind(this))
        net.events.on('connected', () => {
            net.io.emit('join', 'arena')
        })
        net.connect()

        this.input.setPollAlways()
        this.cursors = this.input.keyboard.createCursorKeys()

        // Setup drawing targeting line
        this.lastMouse = { x: this.input.activePointer.worldX, y: this.input.activePointer.worldY, initialized: false }
        this.lastAvatar = { x: -1, y: -1 }
        this.targetLine = this.add.sprite(0, 0, 'line').setOrigin(0, 0).setTint(0x559955, .5).setAlpha(.2).setDepth(1)

        this.accumMS = 0
        this.hzMS = 1 / 60 * 1000
        this.world = new PL.World(Vec2(0, 30))

        this.sky = this.add.tileSprite(0, 0, this.cameras.main.width, this.cameras.main.height, 'sky')
            .setOrigin(0)
            .setScrollFactor(0, 0)
            .setDepth(0)
    },

    onJoinSuccess: function(data) {
        this.ents.myId = data.me.id
        this.ents.myColor = data.me.color
        this.ents.map = data.map
        this.ents.map.bounds.x *= this.ents.scaleFactor
        this.ents.map.bounds.y *= this.ents.scaleFactor
        this.ents.map.bounds.w *= this.ents.scaleFactor
        this.ents.map.bounds.h *= this.ents.scaleFactor

        this.ents.make('bounds', data.map.bounds)
        this.events.emit('joinSuccess')

        _.each(data.ents, e => this.ents.make(e.type, _.mergeWith({}, e, (dest, serverVal, key) => {
            if (this.ents.scalables[key])
                return serverVal * this.ents.scaleFactor
        })))

        let avatar = _.find(this.ents.ents, t => t.id === this.ents.myId)
        this.ents.me = avatar
        this.cameras.main.startFollow(this.ents.me._sprites[0], true, .15, .15)
            .setDeadzone(50, 35)
        this.updates = 0

        this.lastMouse.x = avatar.x
        this.lastMouse.y = avatar.y
        this.input.once('pointermove', ptr => {
            this.lastMouse.initialized = true
        })
    },

    onWorldUpdate: function(data) {
        let ent
        for (var i = 0; i < data.ents.length; i++) {
            let e = _.mergeWith({}, data.ents[i], (dest, serverVal, key) => {
                if (this.ents.scalables[key])
                    return serverVal * this.ents.scaleFactor
            })
            if (e.r == 450) debugger
            ent = this.ents.byId[e.id]
            if (ent) {
                this.events.emit('netUpdate', ent, e)
            } else {
                let w = this.ents.make(e.type, e)
                w.netState = e
            }
        }
        _.each(data.events, id => {
            this.ents.remove(id)
        })
    },

    update: function(time, delta) {
        this.accumMS += delta
        if (this.accumMS >= this.hzMS) {
            if (this.input.activePointer.worldX !== this.lastMouse.x || this.input.activePointer.worldY !== this.lastMouse.y) {
                if (this.lastMouse.initialized) {
                    net.io.emit('move', { x: this.input.activePointer.worldX / this.ents.scaleFactor, y: this.input.activePointer.worldY / this.ents.scaleFactor })
                    this.lastMouse.x = this.input.activePointer.worldX
                    this.lastMouse.y = this.input.activePointer.worldY
                }
            }
        }
        while (this.accumMS >= this.hzMS) {
            this.accumMS -= this.hzMS
            this.world.step(1 / 60)
        }
        this.render.update(delta)
        this.updateDestTarget()
        this.sky.setTilePosition(this.cameras.main.scrollX * .1, this.cameras.main.scrollY * .1)
    },

    updateDestTarget: function() {
        if (this.ents.me) {
            this.targetLine.setScale(Phaser.Math.Distance.Between(this.ents.me.x, this.ents.me.y, this.lastMouse.x, this.lastMouse.y) / 10, 3)
                .setRotation(Phaser.Math.Angle.BetweenPoints(this.ents.me, this.lastMouse))
                .setPosition(this.ents.me.x, this.ents.me.y)
        }
    },

})