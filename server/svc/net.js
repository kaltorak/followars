var _ = require('lodash')
var admin = require('./firebase')
var util = require('util')
var EventEmitter = require('eventemitter3')

function Net(io) {
    EventEmitter.call(this)
    this.io = io
    this.connections = {}

    io.on('connection', this.onConnection.bind(this))
}
util.inherits(Net, EventEmitter)

Net.prototype.send = function(cmd, msg, user) {
    this.connections[user.uid].emit(cmd, msg)
}

Net.prototype.broadcast = function(cmd, msg) {
    this.io.emit(cmd, msg)
}

Net.prototype.broadcastExcept = function(cmd, msg, user) {
    console.log('broadcasting except self ' + cmd)
    this.connections[user.uid].broadcast.emit(cmd, msg)
}

Net.prototype.onConnection = function(socket) {
    console.log('user connected', socket.id)
    socket.on('authRequest', token => {
        admin.auth().verifyIdToken(token)
            .then(decodedToken => {
                socket.user = decodedToken
                socket.authed = true
                this.connections[socket.user.uid] = socket

                this.addListeners(socket)

                socket.emit('authSuccess')
                this.emit('joined', null, socket.user)
            }).catch(function(error) {
                socket.emit('authFailure', error.toString())
                console.log(error)
            })
    })
}

Net.prototype.addListeners = function(socket) {
    _.each(['move' ], cmd => {
        socket.on(cmd, msg => {
            if (!socket.authed) return console.log('ERROR NOT AUTHED')
            this.emit(cmd, msg, socket.user)
        })
    })
}

module.exports = Net