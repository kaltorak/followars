var _ = require('lodash')
var PL = require('planck-js')
let Vec2 = PL.Vec2

var lookup = {
    polygon: function(ent) {
        return PL.Polygon(ent.geom)
    },
    box: function(ent) {
        return PL.Box(ent.w / 2, ent.h / 2)
    },
    chain: function(ent) {
        return PL.Chain(ent.geom, true)
    },
    circle: function(ent) {
        return PL.Circle(ent.r)
    }
}

function World(server) {
    this.events = server.events
    this.ents = server.ents
    this.events.on('entCreated', this.onEntCreated.bind(this))
    this.events.on('entRemoved', this.onEntRemoved.bind(this))

    this.shortlist = []
    this.elapsed = 0

    _.merge(this, {
        bounds: {
            x: 0,
            y: 0,
            w: 96,
            h: 56
        },
        cellSize: 8,
        padding: 2,
    })

    this.accum = 0
    this.world = new PL.World(Vec2(0, 0))

    this.generate()
    this.destroyQueue = []
    this.queueForDestroy = function(body) {
        this.destroyQueue.push(body)
    }
    this.world.on('pre-solve', (contact, oldManifold) => {
        var manifold = contact.getManifold()
        if (manifold.pointCount == 0) return

        this.events.emit('collision', contact.getFixtureA(), contact.getFixtureB(), this.queueForDestroy.bind(this))
    })
}

/*
    Victory condition is capturing all the map obstacles
    Capture spawns by moving near them
        tradeoff of capturing spawns for more troops, vs map for victory
    Map obstacles grant map vision when captured
 */

World.prototype.generate = function() {
    this.spawns = []
    let shapes = []
    shapes.push([Vec2(-3, 3), Vec2(3, 3), Vec2(3, -3), Vec2(-3, -3)])
    shapes.push([Vec2(0, 0), Vec2(-3, 3), Vec2(3, 3), Vec2(3, -3), Vec2(-3, -3)])
    shapes.push([Vec2(-3, 3), Vec2(0, 0), Vec2(3, 3), Vec2(3, -3), Vec2(-3, -3)])
    shapes.push([Vec2(-3, 3), Vec2(3, 3), Vec2(0, 0), Vec2(3, -3), Vec2(-3, -3)])
    shapes.push([Vec2(-3, 3), Vec2(3, 3), Vec2(3, -3), Vec2(0, 0), Vec2(-3, -3)])
    for (let x = this.cellSize / 2; x < this.bounds.w - this.cellSize; x += this.cellSize) {
        for (let y = this.cellSize / 2; y < this.bounds.h - this.cellSize; y += this.cellSize) {
            let posX = _.random(this.padding, this.cellSize - this.padding, true)
            let posY = _.random(this.padding, this.cellSize - this.padding, true)
            let geom = _.sample(shapes)
            this.ents.make('obstacle', {
                x: x + posX,
                y: y + posY,
                geom: geom,
            })
        }
    }
    for (let x = 0; x <= this.bounds.w; x += this.cellSize) {
        for (let y = 0; y <= this.bounds.h; y += this.cellSize) {
            if ((x == 0 || x == this.bounds.w) || (y == 0 || y == this.bounds.h)) {
                if (Math.random() >= .4)
                    this.ents.make('spawn', {
                        _spawnX: x ? x - .5 : .5,
                        _spawnY: y ? y - .5 : .5,
                        x: x,
                        y: y
                    })
            }
        }
    }

    var ground = this.world.createBody({
        position: Vec2(this.bounds.w / 2, this.bounds.h / 2),
        type: 'static'
    })
    ground.createFixture(PL.Chain([
        Vec2(-this.bounds.w / 2, -this.bounds.h / 2),
        Vec2(this.bounds.w / 2, -this.bounds.h / 2),
        Vec2(this.bounds.w / 2, this.bounds.h / 2),
        Vec2(-this.bounds.w / 2, this.bounds.h / 2)
    ], true))
}

//doubles for a respawn method
World.prototype.spawnAvatar = function(player) {
    let avatar = _.find(this.ents.getBy('avatar'), ent => ent.id === player.id)
    if (!avatar) {
        avatar = this.ents.make('avatar', {
            id: player.id,
            ownerId: player.id,
            color: player.color
        })
    }
    player.avatarId = avatar.id
    let searches = 0
    let searching = true
    let x = 0
    let y = 0
    let obstacles = this.ents.getBy('obstacle')
    while (searching) {
        searches++
        x = _.random(2, this.bounds.w - 1)
        y = _.random(2, this.bounds.h - 1)
        //make sure we're not too close to obstacles, or the map border
        searching = _.some(obstacles, o => Math.hypot(x - o.x, y - o.y) < 3 + (1.5 * 1.414)) ||
            (x <= 1 || x > this.bounds.w - 1) || (y <= 1 || y > this.bounds.h - 1)

    }

    avatar.chain('x', x)
        .chain('y', y)
        .chain('_destX', x)
        .chain('_destY', y)
        ._body.setPosition(Vec2(x, y))
}

World.prototype.onMove = function(msg, player) {
    let avatar = this.ents.byId[player.avatarId]
    avatar.set('_destX', msg.x)
    avatar.set('_destY', msg.y)
}

let bounds = new PL.AABB()
let results = []
World.prototype.getEntsIn = function(minx, miny, maxx, maxy, filter) {
    bounds.lowerBound.x = minx
    bounds.lowerBound.y = miny
    bounds.upperBound.x = maxx
    bounds.upperBound.y = maxy
    results.length = 0
    this.world.queryAABB(bounds, (fix) => {
        if (fix.m_userData && filter && filter(fix.m_userData))
            results.push(fix.m_userData)
        return true
    })
    return results
}

World.prototype.onEntCreated = function(ent) {
    if (ent.geomType) {
        ent._body = this.world.createBody({
            position: Vec2(ent.x, ent.y),
            type: ent.bodyType,
            linearDamping: 8,
        })
        ent._body.createFixture(lookup[ent.geomType](ent), {
            friction: 0,
            restitution: 0,
            density: .005,
            userData: ent
        })
    }
}

World.prototype.onEntRemoved = function(ent) {
    if (ent._body)
        this.world.destroyBody(ent._body)
}

World.prototype.update = function(delta) {

    this.world.step(delta)
    this.world.clearForces()
    for (let i = this.destroyQueue.length; i--;) {
        this.world.destroyBody(this.destroyQueue[i])
    }
    this.destroyQueue.length = 0
}

module.exports = World