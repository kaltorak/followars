var _ = require('lodash')
var Victor = require('victor')
var PL = require('planck-js')
let Vec2 = PL.Vec2
let AABB = PL.AABB

function Army(server) {
    this.ents = server.ents
    this.world = server.world
    this.events = server.events;
    this.events.on('entCreated', this.onEntCreated.bind(this))
    this.events.on('entRemoved', this.onEntRemoved.bind(this))

    this.shortlist = []
    this.projectiles = []
    this.elapsed = 0

    this.events.on('collision', this.onCollision, this)
}

Army.prototype.update = function(delta) {
    this.updateSpawns(delta)
    this.updateArmies(delta)
    // this.updateBullets(delta)
    this.updateMap(delta)
}

Army.prototype.updateSpawns = function(delta) {
    let spawns = this.ents.getBy('spawn')
    let avatars = this.ents.getBy('avatar')
    let near = []
    for (let i = spawns.length; i--;) {
        let spawn = spawns[i]

        if (spawn.ownerId) {
            spawn._cool = Math.max(spawn._cool - delta, 0)
            if (spawn._cool === 0 && spawn._hp === spawn.maxHp) {
                this.ents.make(spawn.unitType, {
                    ownerId: spawn.ownerId,
                    x: spawn._spawnX,
                    y: spawn._spawnY
                })
                spawn._cool = spawn.cooldown
                spawn.set('cooldown', spawn.cooldown, true)
            }
        }
        near.length = 0

        for (let a = avatars.length; a--;) {
            let av = avatars[a]
            if (Math.hypot(av.x - spawn.x, av.y - spawn.y) < av.r + spawn.r) {
                near.push(av)
            }
        }

        if (near.length > 1)
            spawn.set('contested', true)
        if (near.length <= 1)
            if (spawn.contested)
                spawn.set('contested', false)

        if (!spawn.contested)
            if (spawn._hp < spawn.maxHp) {
                spawn.set('_hp', Math.min(spawn._hp + (delta * spawn.maxHp / 10), spawn.maxHp))
                spawn.set('hp', Math.round(spawn._hp * 10) / 10)

            }

        if (near.length === 1) {
            if (spawn.ownerId !== near[0].ownerId) {
                spawn.set('_hp', Math.max(spawn._hp - (delta * spawn.maxHp / 2.5), 0))
                spawn.set('hp', Math.round(spawn._hp * 10) / 10)
                if (spawn._hp === 0) {
                    spawn.set('ownerId', near[0].ownerId)
                    spawn._cool = spawn.cooldown
                    spawn.set('cooldown', spawn.cooldown, true)
                    console.log(`${near[0].id} captured spawn ${spawn.id}`)
                }
            }
        }
    }
}


Army.prototype.updateArmies = function(delta) {
    /*
      if units are outside player voice range, they move toward player
      if units are inside player voice range 
        prioritize other units
        then map obstacle closest to mouse

      units outside range decay from sadness, if they take too long getting to the player, they die
    */

    let avatars = this.ents.getBy('avatar')

    for (let i = 0; i < avatars.length; i++) {
        const av = avatars[i]
        let pos = av._body.getWorldCenter()
        if (Math.hypot(av.x - av._destX, av.y - av._destY) > 1) {
            let dir = Vec2(av._destX - pos.x, av._destY - pos.y)
            dir.normalize()
            av._body.applyForceToCenter(dir.mul(25 * delta), true);
        }
        av.set('x', pos.x)
        av.set('y', pos.y)

        // get the map obstacle under the mouse
        // there is a quirk with chain shapes being decomposed into AABB proxies, so the entirety of the chain is not a single AABB
        // this leads to some AABB checks falling through the cracks.
        let mapTarget = this.world.getEntsIn(av._destX - 3, av._destY - 3, av._destX + 3, av._destY + 3,
            (ent) => ent.ownerId !== av.ownerId && ent.type === 'obstacle' && Math.hypot(ent.x - av._destX, ent.y - av._destY) < 3.5);
        mapTarget = _.minBy(mapTarget, t => Math.hypot(t.x - av._destX, t.y - av._destY))

        let nearbyEnemies = this.world.getEntsIn(av.x - 10, av.y - 10, av.x + 10, av.y + 10,
            (ent) => ent.ownerId !== av.ownerId && ent.type === 'soldier')

        let army = this.ents.getBy(av.ownerId)

        for (let s = 0; s < army.length; s++) {
            let unit = army[s]
            let moveTarget = av
            if (Math.hypot(av.x - unit.x, av.y - unit.y) < av.r + unit.r + av.voiceRadius) {
                unit.set('_sadness', 0)
                unit.set('sadness', 0)
                moveTarget = _.minBy(nearbyEnemies, ne => (unit.x - ne.x) * (unit.x - ne.x) + (unit.y - ne.y) * (unit.y - ne.y))
                if (moveTarget || mapTarget) // store the target and dist for attack processing
                    unit._attackTarget = (moveTarget || mapTarget).id
                moveTarget = moveTarget || mapTarget || av
            } else {
                unit._sadness += delta
                unit.set('sadness', Math.trunc(unit._sadness)) // optimize to net send on whole numbers
                if (unit._sadness >= unit.sadLimit) {
                    this.ents.remove(unit)
                    continue
                }
            }

            this.moveToward(unit, moveTarget, delta)

            let upos = unit._body.getPosition()
            unit.set('x', upos.x)
            unit.set('y', upos.y)
        }
    }
}

Army.prototype.moveToward = function(ent, dest, delta) {
    let dir = Vec2(dest.x - ent.x, dest.y - ent.y)
    dir.normalize()
    ent._body.applyForceToCenter(dir.mul(ent._speed * delta), true);
}

Army.prototype.updateBullets = function(delta) {
    // let bullet
    // let defender
    // let direction = new Victor(0, 0)
    // for (let i = this.projectiles.length; i--;) {
    //     bullet = this.projectiles[i];
    //     defender = this.ents.byId[bullet.defenderId]
    //     if (!defender || defender._dead) {
    //         this.ents.remove(bullet)
    //         continue
    //     }

    //     direction.x = defender.x
    //     direction.y = defender.y
    //     direction.copy(defender._position)
    //         .subtract(bullet._position)
    //         .normalize()
    //     bullet._position.addScalarX(direction.x * delta * bullet.speed)
    //     bullet._position.addScalarY(direction.y * delta * bullet.speed)
    //     if (defender._position.distance(bullet._position) < 9) {
    //         defender.set('_hp', defender._hp - bullet.damage)
    //         if (defender._hp <= 0)
    //             this.ents.remove(defender)
    //         this.ents.remove(bullet)
    //         continue
    //     }
    //     bullet.set('x', bullet._position.x)
    //     bullet.set('y', bullet._position.y)
    // }
}

Army.prototype.isTouching = function(unit, target) {
    if (target.geom) {

    }
}

Army.prototype.handleAttack = function(attacker, defender, scheduleDestroy) {
    //attacks only happen between soldier/soldier and soldier/obstacle
    if (defender.type === 'obstacle') {
        if (defender.ownerId === attacker.ownerId) return
        defender.set('_hp', Math.max(defender._hp - 1, 0))
        defender.set('hp', Math.round(defender._hp))

        if (defender._hp === 0) {
            defender.set('ownerId', attacker.ownerId)
        }
        scheduleDestroy(attacker._body)
        this.ents.remove(attacker)
    } else {
        scheduleDestroy(attacker._body)
        scheduleDestroy(defender._body)
        this.ents.remove(attacker)
        this.ents.remove(defender)
    }
}

Army.prototype.onCollision = function(fixA, fixB, scheduleDestroy) {
    if (!fixA.m_userData || !fixB.m_userData) return

    if (fixA.m_userData._attackTarget &&
        fixA.m_userData._attackTarget === fixB.m_userData.id) {
        this.handleAttack(fixA.m_userData, fixB.m_userData, scheduleDestroy)
    }
    if (fixB.m_userData._attackTarget &&
        fixB.m_userData._attackTarget === fixA.m_userData.id) {
        this.handleAttack(fixB.m_userData, fixA.m_userData, scheduleDestroy)
    }
}

Army.prototype.updateMap = function(delta) {
    let obstacles = this.ents.getBy('obstacle')
    for (let i = 0; i < obstacles.length; i++) {
        ob = obstacles[i]
        if (ob._hp < ob.maxHp) {
            ob.set('_hp', Math.min(ob.maxHp, ob._hp + delta * .5))
            ob.set('hp', Math.round(ob._hp))
        }
    }
}

Army.prototype.onEntCreated = function(ent) {
    if (ent.hasOwnProperty('_hp')) {
        this.shortlist.push(ent)
    }
    if (ent.type === 'projectile') {
        this.projectiles.push(ent)
    }
    if (ent.type === 'soldier' || ent.type === 'archer') {
        this.ents.setBy(ent.ownerId, ent)
    }

    if (ent.type === 'spawn') {
        ent._cool = ent.cooldown
    }

}

Army.prototype.onEntRemoved = function(ent) {
    // let idx = this.short.indexOf(ent)
    // this.ents.removeBy(ent.roadId, ent)
    // // console.log('removing ', ent.type, ent.id)
    // if (idx > -1) {
    //     this.Army.splice(idx, 1);
    // }
    // idx = this.projectiles.indexOf(ent)
    // if (idx > -1) {
    //     this.projectiles.splice(idx, 1);
    // }
    // idx = this.attackables.indexOf(ent)
    // if (idx > -1) {
    //     this.attackables.splice(idx, 1);
    // }
}

module.exports = Army;