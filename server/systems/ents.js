var _ = require('lodash')
var Ent = require('../ents/ent')
let Victor = require('victor')

function Ents(server) {
    this.events = server.events
    this.ents = []
    this.byId = {}
    this.indexes = {}
}

Ents.prototype.make = function(type, options) {
    //todo further slice and dice by various ID fields
    var ent = new Ent(type, options)
    // console.log('make', type, ent.id)

    this.ents.push(ent)
    this.byId[ent.id] = ent;
    this.setBy(ent.type, ent)
    if (ent.hasOwnProperty('x') && ent.hasOwnProperty('y'))
        ent._position = new Victor(ent.x, ent.y)
    this.events.emit('entCreated', ent)
    return ent
}

Ents.prototype.remove = function(ent) {
    if (ent._dead) return
    if (typeof ent === 'string')
        ent = this.byId[ent]
    ent._dead = true
    this.events.emit('entRemoving', ent)
    let idx = this.ents.indexOf(ent)
    if (idx > -1) {
        this.ents.splice(idx, 1);
    }

    delete this.byId[ent.id]
    this.events.emit('entRemoved', ent)
}

Ents.prototype.setBy = function(key, val) {
    if (!this.indexes[key]) this.indexes[key] = []
    this.indexes[key].push(val)
}

Ents.prototype.getBy = function(key) {
    return this.indexes[key] || []
}

Ents.prototype.removeBy = function(key, val) {
    if (!this.indexes[key]) return
    let idx = this.indexes[key].indexOf(val)
    if (idx > -1) {
        this.indexes[key].splice(idx, 1);
    }
}

Ents.prototype.clearBy = function(key) {}

module.exports = Ents;