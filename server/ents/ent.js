let _ = require('lodash')

const shortid = require('shortid');
let types = require('./types')

function Ent(type, options) {
    _.merge(this, {
        id: shortid.generate(),
        type: type,
        _dirty: true,
        _dirtyFlags: {}
    }, _.clone(types[type]), options)

    _.forOwn(this, (val, key, obj) => {
        if (key[0] !== '_')
            obj._dirtyFlags[key] = true
    })
    return this
}

Ent.prototype.setAllDirty = function() {
    _.forOwn(this._dirtyFlags, (val, key) => this._dirtyFlags[key] = true)
    this._dirty = true
}

Ent.prototype.setDirty = function(prop) {
    this._dirtyFlags[prop] = true
    this._dirty = true
}

Ent.prototype.set = function(prop, val, forceDirty) {
    if (this[prop] === val && !forceDirty) return val // don't update if no change 

    this[prop] = val
    if (this._dirtyFlags.hasOwnProperty(prop)) {
        this._dirtyFlags[prop] = true
        this._dirty = true
    }
    return val
}

//Overload to allow fluent/chaining
Ent.prototype.chain = function(prop, val, forceDirty) {
    if (this[prop] === val && !forceDirty) return val

    this[prop] = val
    if (this._dirtyFlags.hasOwnProperty(prop)) {
        this._dirtyFlags[prop] = true
        this._dirty = true
    }
    return this
}


module.exports = Ent