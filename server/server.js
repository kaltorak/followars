var _ = require('lodash')
var World = require('./systems/world')
var Army = require('./systems/army')
var Ents = require('./systems/ents')
var randomcolor = require('randomcolor')
var shortid = require('shortid')
var EventEmitter = require('eventemitter3')
// evolutionary feedback loops in a closed system of self differencing

function Server(net) {
    this.events = new EventEmitter()
    this.name = "FolloWars"
    this.players = []

    this.net = net
    this.ents = new Ents(this)
    this.world = new World(this)
    this.army = new Army(this)

    this.elapsed = 0

    this.worldDelta = {
        ents: [],
        events: []
    }
    this.worldEvents = []


    net.on('joined', this.onUserJoined.bind(this))
    net.on('move', (msg, user) => this.world.onMove(msg, this.resolvePlayer(user)))
    this.events.on('entRemoved', ent => this.worldEvents.push(ent.id))

    this.start()
}

Server.prototype.start = function() {
    const hrtimeMs = function() {
        let time = process.hrtime()
        return time[0] * 1000 + time[1] / 1000000
    }

    const TICK_RATE = 40
    let tick = 0
    let previous = hrtimeMs()
    let tickLengthMs = 1000 / TICK_RATE
    let accum = 0
    let hz = 1 / TICK_RATE
    const loop = () => {
        setTimeout(loop, tickLengthMs)
        let now = hrtimeMs()
        accum += (now - previous) / 1000
        while (accum >= hz) {
            accum -= hz
            this.update(hz)
        }
        previous = now
        tick++
    }
    loop() // starts the loop
}

Server.prototype.onUserJoined = function(data, user) {
    console.log('user joining', user)
    var player = this.resolvePlayer(user)
    if (player) {
        player.status = 'joining'
    } else {
        player = {
            name: 'USERNAME',
            id: shortid.generate(),
            uid: user.uid,
            status: 'joining',
            color: parseInt('0x' + randomcolor().slice(1))
        }

        this.players.push(player)
        this.world.spawnAvatar(player)
    }

    this.net.send('joinSuccess', this.buildFullState(player), player)
    player.status = 'playing'
}

Server.prototype.resolvePlayer = function(user) {
    return _.find(this.players, p => p.uid === user.uid)
}

Server.prototype.buildFullState = function(player) {
    let fullState = {
        me: {
            id: player.id,
            color: player.color
        },
        map: {
            bounds: this.world.bounds,
        },
        ents: []
    }

    //TODO run a vision filter pass on all entities subject to warfog
    let ent
    for (var i = 0; i < this.ents.ents.length; i++) {
        ent = this.ents.ents[i]
        var e = new Object()
        for (var prop in ent._dirtyFlags)
            e[prop] = ent[prop]
        fullState.ents.push(e)
    }

    return fullState
}

Server.prototype.buildDirty = function() {
    this.worldDelta.ents.length = 0
    this.worldDelta.events = this.worldEvents
    this.worldEvents = []

    //need to run vision filter pass here also
    let ent
    for (var i = 0; i < this.ents.ents.length; i++) {
        ent = this.ents.ents[i]
        if (ent._dirty) {
            var e = new Object()
            e.id = ent.id
            for (var prop in ent._dirtyFlags) {
                if (ent._dirtyFlags[prop]) {
                    e[prop] = ent[prop]
                    ent._dirtyFlags[prop] = false
                }
            }
            this.worldDelta.ents.push(e)
            ent._dirty = false;
        }
    }
}

Server.prototype.update = function(delta) {
    this.elapsed += delta
    while (this.elapsed >= delta) {
        this.world.update(delta)
        this.army.update(delta)
        this.elapsed -= delta
    }
    this.buildDirty()
    if (this.worldDelta.ents.length ||
        this.worldDelta.events.length) {
        this.net.broadcast('worldUpdate', this.worldDelta)
    }
}

module.exports = Server